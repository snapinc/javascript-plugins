(function($){
    $.fn.extend({ 
        
        endless_scrolling_slider: function(options) {
 
            // default settings
            var defaults = {
                slides_finished: true,
                slide_speed: 3500,
                slide_interval: 5000,
                easing_type: 'easeInOutQuad',
                arrow_left: '.slider_arrow_left',
                arrow_right: '.slider_arrow_right',
                slide_elements: '.slide'
            };
             
            var options = $.extend(defaults, options);

            // create a reference to the inner slideshow container
            carousel_inner_container = $(this);

            // set css on inner slide container
            slides_width_all = $(options.slide_elements).length * $(options.slide_elements+':eq(0)').width();
            carousel_inner_container.css('width', slides_width_all);

            // get the width of single slides
            slides_width_single = $(this).children(options.slide_elements+':first').css('width');

            // start the auto slideshow
            auto_slide_start = setInterval(auto_slide,options.slide_interval);

            function auto_slide() {
                if (options.slides_finished) {
                    
                    options.slides_finished = false;

                    carousel_inner_container.animate({
                        'margin-left': '-='+this.slides_width_single,
                      }, options.slide_speed, options.easing_type, function() {             
                        options.slides_finished = true;

                        carousel_inner_container.append( $(options.slide_elements).first() );
                        carousel_inner_container.css( 'margin-left','0');
                    });
                }   
            }

            // slider click right
            $(options.arrow_right).on('click',function() {

                clearInterval(auto_slide_start);

                if (options.slides_finished) {

                    options.slides_finished = false;
                    
                    carousel_inner_container.animate({
                        'margin-left': '-='+slides_width_single,
                      }, options.slide_speed, options.easing_type, function() {             
                        options.slides_finished = true;

                        carousel_inner_container.append( $(options.slide_elements).first() );
                        carousel_inner_container.css( 'margin-left','0');
                    });
                }       
            });

            // slider click left
            $(options.arrow_left).on('click',function() {

                clearInterval(auto_slide_start);

                if (options.slides_finished) {

                    options.slides_finished = false;
                    
                    carousel_inner_container.prepend( $(options.slide_elements).last() );
                    carousel_inner_container.css('margin-left','-'+slides_width_single);

                    carousel_inner_container.animate({
                        'margin-left': '0',
                      }, options.slide_speed, options.easing_type, function() {
                        options.slides_finished = true;
                    });
                }       
            });
        }
    });
})(jQuery);

/* example usage 

$(document).ready(function() {

    $('.slider_container_inner').endless_scrolling_slider({
        easing_type     : 'easeInOutQuad',
        arrow_left      : '.slider_arrow_left',
        arrow_right     : '.slider_arrow_right',
        slide_speed     : 350,
        slide_duration  : 6000
    });
});

*/
